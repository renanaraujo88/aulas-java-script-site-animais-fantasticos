﻿
//NAVEGAÇÃO POR TABBBBBBB

function initTabNav() {

    const tabMenu = document.querySelectorAll(".js-tabmenu li");
    const tabContent = document.querySelectorAll(".js-tabcontent section");

    if (tabMenu.length && tabContent.length) {
        tabContent[0].classList.add('ativo');

        function activeTab(index) {
            tabContent.forEach((section) => {
                section.classList.remove('ativo')
            })

            tabContent[index].classList.add('ativo');
        }

        tabMenu.forEach((itemMenu, index) => {
            itemMenu.addEventListener('click', () => {
                activeTab(index)
            })
        })
    }
}

initTabNav();


//ACCORDION LIST

function initAccordion() {

    const accordionList = document.querySelectorAll('.js-accordion dt');
    const activeClass = 'ativo';

    if (accordionList.length) {

        accordionList[0].classList.add(activeClass);
        accordionList[0].nextElementSibling.classList.add(activeClass);

        function activeAccordion() {
            this.classList.toggle(activeClass);
            this.nextElementSibling.classList.toggle(activeClass);
        }

        accordionList.forEach((item) => {
            item.addEventListener('click', activeAccordion);
        })
    }
}

initAccordion();

//Constructor function

// Transforme o objeto abaixo em uma Constructor Function

const pessoa = {
    nome: 'Nome pessoa',
    idade: 0,
    andar() {
        console.log(this.nome + 'andou')
    }
}

function Pessoa(nome, idade) {
    this.nome = nome;
    this.idade = idade;

    this.andar = function () {
        console.log(this.nome + 'andou')
    }
}

//Crie 3 pessoas, João - 20 anos,
//Maria - 25 anos, Bruno - 15 anos..

const joao = new Pessoa('João', 20);
const maria = new Pessoa('Maria', 25);
const bruno = new Pessoa('Bruno', 15);




//STRINGGGGG MANIPULAÇÃO


// Utilizando o foreach na array abaixo,
// some os valores de Taxa e os valores de Recebimento

const transacoes = [
    {
        descricao: 'Taxa do Pão',
        valor: 'R$ 39',
    },
    {
        descricao: 'Taxa do Mercado',
        valor: 'R$ 129',
    },
    {
        descricao: 'Recebimento de Cliente',
        valor: 'R$ 99',
    },
    {
        descricao: 'Taxa do Banco',
        valor: 'R$ 129',
    },
    {
        descricao: 'Recebimento de Taxa Cliente',
        valor: 'R$ 49',
    },
];


let taxaTotal = 0;
let recebimentoTotal = 0;

transacoes.forEach((item) => {
    const numeroLimpo = +item.valor.replace("R$ ", "");
    if (item.descricao.slice(0, 4) === 'Taxa') {
        taxaTotal += numeroLimpo;
    } else {
        recebimentoTotal += numeroLimpo;
    }
})

console.log(taxaTotal);
console.log(recebimentoTotal);

// Retorne uma array com a lista abaixo
const transportes = 'Carro;Avião;Trem;Ônibus;Bicicleta';

const arrayTransportes = transportes.split(";");

console.log(arrayTransportes);


// Substitua todos os span's por a's
let html = `<ul>
                <li><span>Sobre</span></li>
                <li><span>Produtos</span></li>
                <li><span>Contato</span></li>
              </ul>`;


// Retorne o último caracter da frase


// Retorne o total de taxas
const transacoes2 = ['Taxa do Banco', '   TAXA DO PÃO', '  taxa do mercado', 'depósito Bancário', 'TARIFA especial'];


//Arrays e iteração.....

const aulas = [

    {
        nome: 'HTML 1',
        min: 15
    },

    {
        nome: 'HTML 2',
        min: 10
    },

    {
        nome: 'CSS 1',
        min: 20
    },

    {
        nome: 'JS 1',
        min: 25
    },
]


//Array reduce

function ReduceAula() {

    const arrayReduce = [10, 25, 30];


    const reduceAulas = arrayReduce.reduce((acumulador, item) => {
        return acumulador + item;
    }, 0)


    console.log(reduceAulas);
}

//Outra forma do reduce...

function ReduceNumeroMaior() {

    const numeros = [10, 25, 30, 3, 54, 33, 22];

    const maiorNumero = numeros.reduce((anterior, atual) => {
        return anterior > atual ? anterior : atual;
    }, 0)

    console.log(maiorNumero);

}

//Array some = Verifica se um array pelo menos é verdadeiro...

function ArraySome() {
    const frutas = ['Banana', 'Pêra', 'Uva'];

    const temUva = frutas.some((item) => {
        return item === 'Uva';
    })

    console.log(temUva);
}

//Array every = Verifica se todos os arrays são verdadeiros...

function ArrayEvery() {
    //String vazia retorna false = exemplo..
    const frutas = ['Banana', 'Pêra', 'Uva', ''];

    const every = frutas.every((item) => {
        return item === 'Uva';
    })

    console.log(every);
}


//Array find = retorna o primeiro valor true que encontrar...

function ArrayFind() {

    const frutasFind = ['Banana', 'Pêra', 'Uva', 'Maça'];

    const indexUva = frutasFind.find((item) => {
        return item === "Uva";
    })

    console.log(indexUva);
}

//Array findIndex = retorna o primeiro valor true que encontrar pelo index

function ArrayFindIndex() {

    const frutasFindIndex = ['Banana', 'Pêra', 'Uva', 'Maça'];

    const indexUva = frutasFindIndex.find((item) => {
        return item === "Uva";
    })

    console.log(indexUva);
}


//Array filter = retorna uma array com a lista de valores que forem true...

function ArrayFilter() {

    const frutasFilter = ['Banana', undefined, null, '', 'Pêra', 0, 'Uva', 'Maça'];

    const arrayLimpa = frutasFilter.filter((fruta) => {
        return fruta;
    })

    console.log(arrayLimpa);

    //Com numeros...

    const numeros = [6, 43, 22, 88, 101, 29];

    const buscaMaior45 = numeros.filter(x => x > 45);

    console.log(buscaMaior45);
}


function initModal() {

    const botaoAbrir = document.querySelector('[data-modal="abrir"]');
    const botaoFechar = document.querySelector('[data-modal="fechar"]');
    const containerModal = document.querySelector('[data-modal="container"]');


    if (botaoAbrir && botaoFechar && containerModal) {

        function toogleModal(event) {
            event.preventDefault();
            containerModal.classList.toggle('ativo');
        }

        function cliqueForaModal(event) {
            if (event.target === this)
                toogleModal(event);
        }

        botaoAbrir.addEventListener('click', toogleModal);
        botaoFechar.addEventListener('click', toogleModal);
        containerModal.addEventListener('click', cliqueForaModal);
    }

}

initModal();

/*Aprendendo Data Object*/

const agora = new Date();
const futuro = new Date("Dec 24 2022");

console.log(agora);
console.log(agora.getDate());
console.log(agora.getDay());
console.log(agora.getMonth());

console.log(futuro);

/*O método getTime() mostra o tempo total em milessegundos desde o dia 1 de Janeiro de 1970.*/

function transformarDias(tempo) {
    return tempo / (24 * 60 * 60 * 1000);
}

const diasAgora = transformarDias(agora.getTime());
const diasPromocao = transformarDias(futuro.getTime());

const faltamDias = diasPromocao - diasAgora;


/*Funcionamento da Loja Function*/

function initFuncionamento() {

    const funcionamento = document.querySelector('[data-semana]');
    const diasSemana = funcionamento.dataset.semana.split(',').map(Number);
    const horariosSemana = funcionamento.dataset.horario.split(',').map(Number);

    const dataAgora = new Date();
    const diaAgora = dataAgora.getDay();
    const horarioAgora = dataAgora.getHours();

    /*Verificar o dia da semana para saber se está aberto.*/

    /*Você tem um array dos dias da semana = diasSemana[1,2,3,4,5]..*/

    /*Você tem o dia da semana = diaAgora[]*/

    const semanaAberto = diasSemana.indexOf(diaAgora) !== -1;
    const horarioAberto = (horarioAgora >= horariosSemana[0] && horarioAgora < horariosSemana[1]);

    if (semanaAberto && horarioAberto) {
        funcionamento.classList.add("aberto");
    }
}


initFuncionamento();


/*Forms*/








